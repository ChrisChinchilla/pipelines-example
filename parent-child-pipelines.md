# Parent-child pipelines

Pipelines are one of GitLab's most popular features for automating tasks based on actions contributors take in your project. We are pleased to announce two new features to make Pipelines even more powerful.
---

Matrix pipelines, diffrent targets of architectures you are building. Right now you have to create a job for each (Drone has something similar), Reduces complexity in the YAML file, like a loop in a make file, keywords in YAML can emulate a loop, but it's messy.

Dynamic generation of a child pipeline, like a mono repo. Everyone would have to work in one large YAML file, now devs can have different YAML files in projects and trigger from a top level.

Dynamically generate YAML file and get Gitlab to run it. You can use code to generate these YAML files now. You can generate pretty much everything by triggereing a trigger keyword.

Ruby for generating the YAML, C++ for something else


https://docs.gitlab.com/ee/ci/pipelines/pipeline_architectures.html

https://www.youtube.com/watch?v=nMdfus2JWHM
